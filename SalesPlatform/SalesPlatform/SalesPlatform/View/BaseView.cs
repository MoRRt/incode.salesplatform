﻿using System;
using SalesPlatform.ViewModels;
using Xamarin.Forms;

namespace SalesPlatform.View
{
    public class BaseView : ContentPage
    {
        protected BaseView() : base()
        {
            base.Appearing += (object sender, EventArgs e) => {
                MessagingCenter.Send<BaseViewModel>((BaseViewModel)this.BindingContext, "OnAppearing");
            };

            base.Disappearing += (object sender, EventArgs e) => {
                MessagingCenter.Send<BaseViewModel>((BaseViewModel)this.BindingContext, "OnDisappearing");
            };

            base.LayoutChanged += (object sender, EventArgs e) =>
            {
                MessagingCenter.Send<BaseViewModel>((BaseViewModel)this.BindingContext, "OnLayoutChanged");
            };
        }
    }
}