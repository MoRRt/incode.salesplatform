﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using Xamarin.Forms;

namespace SalesPlatform.ViewModels
{
    public class BaseViewModel : INotifyPropertyChanged
    {
        #region BaseEvents
        protected BaseViewModel()
        {
            MessagingCenter.Subscribe<BaseViewModel>(this, "OnAppearing", (sender) =>
            {
                this.OnAppearing();
            });

            MessagingCenter.Subscribe<BaseViewModel>(this, "OnDisappearing", (sender) =>
            {
                this.OnDisappearing();
            });

            MessagingCenter.Subscribe<BaseViewModel>(this, "OnLayoutChanged", (sender) =>
            {
                this.OnLayoutChanged();
            });

        }
        protected virtual void OnAppearing()
        {

        }

        protected virtual void OnDisappearing()
        {

        }

        protected virtual void OnLayoutChanged()
        {

        }
        #endregion

        #region NFC
        protected void SetProperty<T>(ref T backingStore, T value, [CallerMemberName] string propertyName = "",
            Action onChanged = null)
        {
            if (EqualityComparer<T>.Default.Equals(backingStore, value))
                return;

            backingStore = value;
            onChanged?.Invoke();
            OnPropertyChanged(propertyName);
        }

        #region INotifyPropertyChanged implementation
        public event PropertyChangedEventHandler PropertyChanged;
        #endregion

        public void OnPropertyChanged(string propertyName)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(propertyName));
        }
        #endregion

        private bool busy;
        public const string IsBusyPropertyName = "IsBusy";
        public bool IsBusy
        {
            get
            {
                return busy;
            }
            set
            {
                SetProperty(ref busy, value);
            }
        }
    }
}